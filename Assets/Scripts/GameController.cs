﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameController : MonoBehaviour {


    //Text elements
    public Text levelText;
    public Text timeText;
    public Text speedText;

    //Time tracker
    private float time = 0;

    //Boolean to check if game is running or not
    private bool _gameStarted;
    public bool GameStarted
    {
        get
        {
            return _gameStarted;
        }
        set
        {
            _gameStarted = value;
        }
    }

    // Use this for initialization
    void Start () {
        ResetGame();

        
    }
	
	// Update is called once per frame
	void Update () {

        //Updates the timer in the UI
        if (GameStarted)
        {
            time += Time.deltaTime;
            int minutes = ( int ) time / 60;
            float seconds = ( int ) time % 60;
            timeText.text = "Min:" + minutes.ToString() + " Sec:" + seconds.ToString();
        }

        //Updates the speed and level in the UI
        try
        {
            levelText.text = "Level: " + GameObject.FindGameObjectWithTag("Shape").GetComponent<ShapeHandler>().CurrentLevel;
            speedText.text = "Speed: " + GameObject.FindGameObjectWithTag("Shape").GetComponent<ShapeHandler>().Speed;
        } catch
        {

        }
        
	}

    //Resets the game
    public void ResetGame ()
    {
        GameStarted = false;
        time = 0;
        timeText.text = "0";
        levelText.text = "Level: 0";
        speedText.text = "Speed: 0";
    }
}
