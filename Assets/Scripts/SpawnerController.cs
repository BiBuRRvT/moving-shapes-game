﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpawnerController : MonoBehaviour {

    List<Color> colorList;
    PrimitiveType [] primList = { PrimitiveType.Cube, PrimitiveType.Sphere, PrimitiveType.Cylinder, PrimitiveType.Capsule };


    // Use this for initialization
    void Start () {

        //Make list of available colors
        colorList = new List<Color>();
        Color blue = new Color(0, 0, 1);
        Color red = new Color(1, 0, 0);
        Color green = new Color(0, 1, 0);
        Color yellow = new Color(1, 1, 0);
        colorList.Add(blue);
        colorList.Add(red);
        colorList.Add(green);
        colorList.Add(yellow);
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    //Starts the game
    void OnMouseDown ()
    {
        SetSpawnerVisibility();
        GameObject randomObject = GameObject.CreatePrimitive(PickRandomPrimitive());
        randomObject.transform.position = this.transform.position;
        randomObject.AddComponent<ShapeHandler>();
        randomObject.GetComponent<MeshRenderer>().material.color = PickRandomColor();
        randomObject.tag = "Shape";
    }

    //Picks a random color from the colorlist
    public Color PickRandomColor()
    {
        Color selectedColor = colorList [ Random.Range(0, colorList.Count) ];
        return selectedColor;
    }

    //Picks a random shape from the primitive list
    PrimitiveType PickRandomPrimitive ()
    {
        PrimitiveType selectedPrimType = primList [ Random.Range(0, primList.Length) ];
        return selectedPrimType;
    }

    //Sets the spawner visiblity
    void SetSpawnerVisibility ()
    {
        this.gameObject.SetActive(false);
    }
}
