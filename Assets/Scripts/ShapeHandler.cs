﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ShapeHandler : MonoBehaviour {

    //Random movement values
    private Vector3 randomPosition;
    private Quaternion randomRotation;
    private Vector3 randomScale;

    //speed value
    private float _speed = 1.0f;
    public float Speed
    {
        get
        {
            return _speed;
        }
        set
        {
            _speed = value;
        }
    }

    //Current level and max level with getters/setters
    private int _currentLevel = 1;
    public int CurrentLevel
    {
        get
        {
            return _currentLevel;
        }
        set
        {
            _currentLevel = value;
        }
    }
    private int _maxLevel = 100;
    public int MaxLevel
    {
        get
        {
            return _maxLevel;
        }
    }

    // Use this for initialization
    void Start () {

        SetRandomTarget();

	}
	
	// Update is called once per frame
	void Update () {

        //Check if object reached target
        if (this.transform.position == randomPosition)
        {
            CurrentLevel++;
            //If player reaches tenth level, the speed increases
            if ( CurrentLevel % 10 == 0 )
            {
                Speed += 0.1f;
                Debug.Log("speed increases");
            }

            //Set new random target
            SetRandomTarget();

        }

        //Move object towards target
        transform.position = Vector3.MoveTowards(this.transform.position, randomPosition, Speed * Time.deltaTime);
        transform.rotation = Quaternion.RotateTowards(this.transform.rotation, randomRotation, Speed * Time.deltaTime * 15);
        transform.localScale = Vector3.Lerp(transform.localScale, randomScale, Time.deltaTime * Speed);


    }

    //Player loses the game when the mouse leaves the shape
    void OnMouseExit ( )
    {
        Debug.Log("game over");
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>().ResetGame();
    }

    //Sets the position, rotation and scale of the object
    void SetRandomTarget()
    {
        //sets the position
        var rp = Random.insideUnitSphere * 4;
        rp.z = this.transform.position.z;
        randomPosition = rp;

        //sets the rotation
        randomRotation = Random.rotation;

        //sets the scale
        float x = Random.Range(0.6f, 1.6f);
        float y = Random.Range(0.6f, 1.6f);
        float z = Random.Range(0.6f, 1.6f);
        randomScale = new Vector3(x, y, z);
        
    }

}
